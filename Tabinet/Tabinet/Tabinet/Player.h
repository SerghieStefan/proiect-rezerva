#include "Card.h"

class Player
{
private:
	std::vector<Card> vectorHand;
	std::vector<Card> vectorCollected;
	int tabla;
	int points;
public:
	void setPoints(int p);
	void showHand();
	void giveCard(Card c);
	void addTabla();
	void minusTabla();
	void copyCardsFromHand(std::vector<Card> m);
	void collectCard(Card c);
	std::vector<Card> getHand()  const;
	std::vector<Card> getCollected()  const;
	int getTabla() const;
	int getPoints() const;
	int getSizeCollected() const;

};