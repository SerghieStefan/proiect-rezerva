#include "Card.h"

std::string Card::getValue()
{
	return value;
}

std::string Card::getSymbol()
{
	return symbol;
}

void Card::setValue(std::string val)
{
	value = val;
}

void Card::setSymbol(std::string sym)
{
	symbol = sym;
}

void Card::showCard()
{
	std::cout << value << "-" << symbol << " ";
}

bool Card::vacuta()
{
	std::regex first("(romb)");
	std::regex second("(trefla)");
	if ((regex_match(symbol, first) && value == "10") || ((regex_match(symbol, second) && value == "2")))
		return true;
	else return false;
}
int Card::getValueOfCard(int theCase)
{
	if (theCase == 1)
	{
		if (getValue() == "A")
			return 1;
	}
	else if (theCase == 2)
	{
		if (getValue() == "A")
			return 11;
	}
	if (getValue() == "2")
		return 2;
	if (getValue() == "3")
		return 3;
	if (getValue() == "4")
		return 4;
	if (getValue() == "5")
		return 5;
	if (getValue() == "6")
		return 6;
	if (getValue() == "7")
		return 7;
	if (getValue() == "8")
		return 8;
	if (getValue() == "9")
		return 9;
	if (getValue() == "10")
		return 10;
	if (getValue() == "J")
		return 12;
	if (getValue() == "Q")
		return 13;
	if (getValue() == "K")
		return 14;
}