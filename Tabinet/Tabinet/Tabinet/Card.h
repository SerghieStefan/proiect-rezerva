#pragma once

#include <iostream>
#include <string>
#include <regex>

class Card
{
private:
	std::string value;
	std::string symbol;
public:
	std::string getValue();
	int getValueOfCard(int theCase);
	std::string getSymbol();
	void setValue(std::string val);
	void setSymbol(std::string sym);
	void showCard();
	bool vacuta();
};