#pragma once

#include <iostream>
#include <vector>
#include <string>

#pragma once

#include <iostream>
#include <vector>
#include <string>

#include "Card.h"
#include "Player.h"

class Tabinet
{
private:
	int numberOfPlayers;
	std::vector<Card> package;
	std::vector<Card> board;
	std::vector<Player> players;
	int last;
public:
	Tabinet();
	void inputPlayers(int& number);
	void play();
	void setPackage();
	void firstHand(Player& player);
	void giveCards(Player& player);
	void showHand();
	void stepOfPlayer(Player& player, int index);
	int makingSum(Card c, std::vector<Card> v);
	void configurePoints(Player& player);
	bool maxCards(Player player);
};