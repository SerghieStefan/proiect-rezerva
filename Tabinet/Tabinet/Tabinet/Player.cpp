#include "PLayer.h"

std::vector<Card> Player::getHand() const
{
	return vectorHand;
}

std::vector<Card> Player::getCollected() const
{
	return vectorCollected;
}

void Player::showHand()
{
	std::cout << "-----------------------------------------------------------------" << std::endl;
	std::cout << "You have in hand: " << std::endl;
	for (int index = 0; index < vectorHand.size(); index++)
	{
		vectorHand[index].showCard();
	}
	std::cout << std::endl;
	for (int index = 0; index < vectorHand.size(); index++)
	{
		std::cout << index << "\t ";
	}

	std::cout << std::endl;
	std::cout << "-----------------------------------------------------------------" << std::endl;
}

void Player::giveCard(Card c)
{
	vectorHand.push_back(c);
}

void Player::addTabla()
{
	tabla++;
}

void Player::minusTabla()
{
	tabla--;
}

void Player::copyCardsFromHand(std::vector<Card> hand)
{
	vectorHand = hand;
}

void Player::collectCard(Card c)
{
	vectorCollected.push_back(c);
}

int Player::getTabla() const
{
	return tabla;
}

void Player::setPoints(int p)
{
	points = p;
}

int Player::getSizeCollected() const
{
	return vectorCollected.size();
}

int Player::getPoints() const
{
	return points;
}