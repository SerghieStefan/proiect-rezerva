#include "Tabinet.h"
#include <sstream>
#include <cstring>
#include <algorithm>
#include <ctime> 

Tabinet::Tabinet()
{

}

void Tabinet::inputPlayers(int& number)
{
    std::cout << "---------------------------TABINET---------------------------" << std::endl;
    std::cout << std::endl;
    bool isOk = false;
    std::cout << "Type the number of players: ";
    std::cin >> number;
    if (number == 2 || number == 3 || number == 4)
    {
        isOk = true;
    }
    while (!isOk)
    {
        std::cout << "Something went wrong,numer of players must be between 2-4" << std::endl;
        std::cin >> number;
        if (number == 2 || number == 3 || number == 4)
        {
            isOk = true;
        }
    }

    numberOfPlayers = number;
    for (int index = 0; index < number; index++)
    {
        Player player;
        players.push_back(player);
    }

    setPackage();

}

void Tabinet::setPackage()
{
    for (int index = 0; index < 52; index++)
    {
        Card card;
        package.push_back(card);
    }
    std::string aString;
    for (int index = 0; index < 13; index++)
    {

        package[index].setSymbol("rosie");

        if (index == 0)
            package[index].setValue("A");
        else if (index == 9)
            package[index].setValue("10");
        else if (index == 10)
            package[index].setValue("J");
        else if (index == 11)
            package[index].setValue("Q");
        else if (index == 12)
            package[index].setValue("K");
        else {
            aString = std::to_string(index + 1);
            package[index].setValue(aString);
        }
    }

    for (int index = 0; index < 13; index++)
    {

        package[index + 13].setSymbol("neagra");
        if (index == 0)
            package[index + 13].setValue("A");
        else if (index == 9)
            package[index + 13].setValue("10");
        else if (index == 10)
            package[index + 13].setValue("J");
        else if (index == 11)
            package[index + 13].setValue("Q");
        else if (index == 12)
            package[index + 13].setValue("K");
        else
        {
            aString = std::to_string(index + 1);
            package[index + 13].setValue(aString);
        }
    }

    for (int index = 0; index < 13; index++)
    {

        package[index + 26].setSymbol("trefla");
        if (index == 0)
            package[index + 26].setValue("A");
        else if (index == 9)
            package[index + 26].setValue("10");
        else if (index == 10)
            package[index + 26].setValue("J");
        else if (index == 11)
            package[index + 26].setValue("Q");
        else if (index == 12)
            package[index + 26].setValue("K");
        else
        {
            aString = std::to_string(index + 1);
            package[index + 26].setValue(aString);
        }
    }

    for (int index = 0; index < 13; index++)
    {

        package[index + 39].setSymbol("romb");
        if (index == 0)
            package[index + 39].setValue("A");
        else if (index == 9)
            package[index + 39].setValue("10");
        else if (index == 10)
            package[index + 39].setValue("J");
        else if (index == 11)
            package[index + 39].setValue("Q");
        else if (index == 12)
            package[index + 39].setValue("K");
        else
        {
            aString = std::to_string(index + 1);
            package[index + 39].setValue(aString);
        }
    }
}


void Tabinet::firstHand(Player& player)
{
    int option = 0;
    std::cout << "--------------------PLAYER 0--------------------" << std::endl;
    std::cout << "You will have these cards: " << std::endl;
    for (int index = 0; index < 4; index++)
    {
        package[index].showCard();
    }
    std::cout << std::endl << "You accept them?" << std::endl;
    std::cout << "1- Yes \n 2-No" << std::endl;
    std::cin >> option;
    while (option != 1 && option != 2)
    {
        std::cout << "Something went wrong,try again 1 or 2." << std::endl;
        std::cin >> option;
    }
    if (option == 1)
    {
        for (int index = 0; index < 6; index++)
        {
            Card c = Tabinet::package[0];
            player.giveCard(c);
            package.erase(package.begin());
        }
    }
    else {
        for (int index = 0; index < 4; index++)
        {
            Card card = Tabinet::package[0];
            board.push_back(card);
            package.erase(package.begin());
        }

        for (int index = 0; index < 6; index++)
        {
            Card c = Tabinet::package[0];
            player.giveCard(c);
            package.erase(package.begin());
        }
    }
}

void Tabinet::giveCards(Player& player)
{
    for (int index = 0; index < 6; index++)
    {
        Card c = Tabinet::package[0];
        player.giveCard(c);
        package.erase(package.begin());

    }
}


void Tabinet::showHand()
{
    std::cout << "-------------------- Cards on the board --------------------" << std::endl;
    for (int index = 0; index < board.size(); index++)
    {
        board[index].showCard();
    }
    std::cout << std::endl;
    for (int index = 0; index < board.size(); index++)
    {
        std::cout << index << "\t ";
    }
    std::cout << std::endl;
    std::cout << "------------------------------------------------------------" << std::endl;
}

int Tabinet::makingSum(Card card, std::vector<Card> vectorCards)
{

    int sum;
    int result;
    if (vectorCards.size() == 1)
    {
        if (card.getValueOfCard(2) == vectorCards[0].getValueOfCard(2))
            return 1;
    }

    sum = 0;
    result = 0;
    for (int index = 0; index < vectorCards.size(); index++)
    {
        sum = sum + vectorCards[index].getValueOfCard(1);
    }
    result = card.getValueOfCard(2);
    if (sum == result)
        return 1;


    sum = 0;
    result = 0;
    for (int index = 0; index < vectorCards.size(); index++)
    {
        sum = sum + vectorCards[index].getValueOfCard(2);
    }
    result = card.getValueOfCard(2);
    if (sum == result)
        return 1;


    return 0;

}

void Tabinet::stepOfPlayer(Player& player, int indexP)
{
    if (player.getHand().empty())
        return;
    int option = 0;
    int number = 0;
    int aux = 0;
    bool oktabla = false;
    int okStep = 0;
    std::vector<Card> inHand = player.getHand();
    Card card;
    std::vector<Card> toTake;
    std::cout << "-----------------PLAYER " << indexP << "-----------------" << std::endl;
    showHand();
    player.showHand();
    std::cout << " 1 - Take cards" << std::endl;
    std::cout << " 2 - Put a card down" << std::endl;
    std::cin >> option;
    while (option != 1 && option != 2)
    {
        std::cout << " Type 1 or 2." << std::endl;
        std::cin >> option;
    }

    if (option == 1)
    {
        while (okStep == 0)
        {
            oktabla = false;
            int indexc = -1;
            card.getSymbol() = "";
            card.getValue() = "";
            toTake.clear();
            std::cout << "Witch what card you want to take?(type the index)" << std::endl;
            std::cin >> indexc;

            while (indexc > 5 || indexc < 0)
            {
                std::cout << "Type a valid index." << std::endl;
                std::cin >> indexc;
            }
            card = inHand[indexc];
            std::cout << "Type how many cards you want to take, then the indexes, if you want to make a 'tabla', type 0" << std::endl;
            std::cin >> number;

            if (number < 1 && number > 7)
            {
                std::cout << "Type a valid number." << std::endl;
                std::cin >> number;
            }

            if (number > 0)
            {
                if (number == 1)
                {
                    std::cin >> aux;
                    toTake.push_back(board[aux]);
                }
                else {
                    for (int index = 0; index < number; index++)
                    {
                        std::cin >> aux;
                        toTake.push_back(board[aux]);
                    }
                }
            }
            else {
                oktabla = true;
            }

            if (oktabla == true)
            {
                for (int index = 0; index < board.size(); index++)
                {
                    toTake.push_back(board[index]);
                }
                okStep = makingSum(card, toTake);
            }
            else {
                okStep = makingSum(card, toTake);
            }

            if (okStep == 1)
            {
                last = indexP;
                if (oktabla)
                {
                    player.addTabla();
                }
                player.collectCard(inHand[indexc]);
                inHand.erase(inHand.begin() + indexc);
                if (toTake.size() == 1)
                {
                    for (int index2 = 0; index2 < board.size(); index2++)
                        if (toTake[0].getValue() == board[index2].getValue() && toTake[0].getSymbol() == board[index2].getSymbol())
                        {
                            player.collectCard(toTake[0]);
                            board.erase(board.begin() + index2);
                            toTake.erase(toTake.begin());
                            break;
                        }
                }
                else {
                    bool okToTake = true;
                    while (!toTake.empty())
                    {
                        for (int index2 = 0; index2 < board.size(); index2++)
                            if (okToTake == true)
                            {
                                if (toTake[0].getValue() == board[index2].getValue() && toTake[0].getSymbol() == board[index2].getSymbol())
                                {
                                    player.collectCard(toTake[0]);
                                    board.erase(board.begin() + index2);
                                    toTake.erase(toTake.begin());
                                    if (toTake.empty())
                                        okToTake = false;
                                }
                            }
                    }
                }

            }
        }
    }
    else {
        int indexc2 = -1;
        std::cout << "Type the index of the card who you want to put down: " << std::endl;
        std::cin >> indexc2;
        while (inHand.size() < indexc2 || indexc2 < 0)
        {
            std::cout << "Wrong input,type other number" << std::endl;
            std::cin >> indexc2;
        }
        board.push_back(inHand[indexc2]);
        inHand.erase(inHand.begin() + indexc2);

    }
    player.copyCardsFromHand(inHand);

}


bool Tabinet::maxCards(Player player)
{
    for (int index = 0; index < players.size(); index++)
        if (player.getSizeCollected() < players[index].getSizeCollected())
            return false;

    return true;
}

void Tabinet::configurePoints(Player& player)
{
    int puncte = 0;
    std::vector<Card> colectat;
    colectat = player.getCollected();
    for (int index = 0; index < colectat.size(); index++)
    {
        if (colectat[index].vacuta())
        {
            puncte = puncte + 2;
        }
        else {
            if (colectat[index].getValueOfCard(1) == 10 || colectat[index].getValueOfCard(1) == 1 || colectat[index].getValueOfCard(2) == 11)
                puncte++;
        }

        if (colectat[index].getSymbol() == "trefla")
        {
            puncte++;
        }
    }

    puncte = puncte + player.getTabla();

    if (maxCards(player) == true)
    {
        puncte = puncte + 3;
    }
    player.setPoints(puncte);

}


void Tabinet::play()
{
    bool ok;
    int winnerIndex = 0;
    std::srand(unsigned(std::time(0)));
    std::cout << "Will be " << players.size() << " players." << std::endl;
    std::random_shuffle(package.begin(), package.end() - 1);

    firstHand(players[0]);
    for (int index = 1; index < players.size(); index++)
    {
        giveCards(players[index]);
    }
    if (board.empty())
    {
        for (int index = 0; index < 4; index++)
        {
            Card card = Tabinet::package[0];
            board.push_back(card);
            package.erase(package.begin());
        }
    }
    while (!package.empty())
    {

        ok = true;
        for (int index = 0; index < players.size(); index++)
        {
            if (!players[index].getHand().empty())
                ok = false;
        }

        if (ok == true)
        {
            if (package.size() / 6 >= numberOfPlayers)
                for (int index = 0; index < players.size(); index++)
                {
                    giveCards(players[index]);
                }
            else {
                while (package.size() > 0)
                {
                    for (int index = 0; index < players.size(); index++)
                    {
                        players[index].giveCard(package[0]);
                        package.erase(package.begin());
                    }
                }
            }
        }
        for (int index = 0; index < players.size(); index++)
        {
            stepOfPlayer(players[index], index);
        }
    }
    ok = true;
    while (ok == true)
    {
        for (int index = 0; index < players.size(); index++)
        {
            if (players[index].getHand().empty())
                ok = false;
        }

        for (int index = 0; index < players.size(); index++)
        {
            stepOfPlayer(players[index], index);
        }
    }

    if (!board.empty())
    {
        for (int index = 0; index < board.size(); index++)
            players[last].giveCard(board[index]);
    }

    for (int index = 0; index < players.size(); index++)
    {
        configurePoints(players[index]);
    }

    for (int index = 0; index < players.size(); index++)
    {
        if (players[winnerIndex].getPoints() < players[index].getPoints())
            winnerIndex = index;
    }

    std::cout << std::endl;
    std::cout << "========================FINAL========================" << std::endl;
    std::cout << std::endl;
    std::cout << "The winner is PLAYER " << winnerIndex << std::endl;
    std::cout << "============================================================" << std::endl;

}